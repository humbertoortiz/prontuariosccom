<center>
Universidad de Puerto Rico
Recinto de Río Piedras<br> Facultad de Ciencias Naturales<br>
Departamento de Ciencia de Cómputos
</center>

---

### Título: Estructura de datos

### Codificación: CCOM 3034

### Número de horas/créditos:  45 horas conferencia, 22.5 horas taller / 3 créditos. 3 hrs conferencia, 1.5 hrs taller (semanales)

### Prerrequisitos: CCOM 3033 aprobado con A o B; MATE 3024

---

### Descripción del curso:

Tipos de datos abstractos, su implantación eficiente y aplicación a la solución de problemas. Introducción al análisis asintótico de algoritmos. Estructuras de datos fundamentales tales como colas, pilas, listas y árboles, su implantación eficiente y aplicaciones. Introducción a “hash-tables”, colas de prioridades y grafos. Técnicas básicas de ordenamiento y búsqueda. Introducción a estructuras de datos concurrentes. El curso tiene tres horas de conferencia a la semana e incluye un taller de una hora y media a la semana. En el taller los estudiantes pondrán en práctica los conceptos aprendidos en la clase.

Abstract data types, their efficient implementation and application to problem solving. Introduction to the asymptotic analysis of algorithms. Fundamental data structures such as queues, stacks, lists and trees, their efficient implementation and applications. Introduction to hash-tables, priority queues, and graphs. Basic sorting and searching methods. Introduction to concurrent data structures. The course has three hours of lecture per week and includes a workshop of one and half hours per week. In the workshop, students will implement the concepts learned in class.

### Objetivos del curso:

Al finalizar el curso el estudiante podrá:

1. Entender y aplicar los conceptos fundamentales de apuntadores y memoria dinámica.

2. Entender y aplicar los conceptos fundamentales de estructuras de datos abstractas

3. Diseñar e implantar en un lenguaje de programación de alto nivel estructuras de datos como  colas, pilas, listas, árboles, “hash-tables” y grafos.

4. Seleccionar la estructura de datos adecuada para solucionar un problema.

5. Entender y aplicar aspectos fundamentales de métodos de búsqueda y ordenamiento.

6. Determinar cuando una técnica de recursión es deseable o requerida.

7. Analizar el tiempo de ejecución asintótico de algoritmos sencillos usando la notación “big-O”

8. Entender los retos presentados por el acceso concurrente a estructuras de datos y las consideraciones elementales y objetivos (scalability, speedup) cuando se implantan las estructuras de datos concurrentes.


### Bosquejo de contenido y distribución del tiempo:

| Tema |  Horas |
|------|--------|
| Repaso de punteros | 1.5 |
| Tipos de Datos Abstractos y su implementación | 6 |
| Listas, colas, pilas | 7.5 |
| Complejidad de algoritmos | 3 |
| Árboles | 6 |
| Recursión | 3 |
| Algoritmos de búsqueda | 3 |
| Heap, Hash Tables | 4.5 |
| Estructuras de datos paralelas/distribuidas | 3 |
| Gráfos | 4.5 |
| Exámenes | 4.5 |
| Total | 45 horas |


### Estrategias instruccionales:
Discusiones, laboratorios, trabajos colaborativos, estudio independiente, lecturas de artículos  de revistas especializadas, entre otros.

### Recursos mínimos disponibles o requeridos:
Salón de computadoras con compilador de C++ y con servicio de Internet

### Estrategias de evaluación:

|  |  Porcentaje |
|------|--------|
|Talleres|20%|
|2 exámenes parciales| 30% (15% cada uno)|
|Examen final|15%|
|1 proyecto en equipo|15%|
|Presentación individual o en grupo, asignaciones, quizes,  participación en clase|10%|
|Asistencia|10%|

Evaluación diferenciada a estudiantes con necesidades especiales.

### Sistema de calificación:
Se utilizará un sistema de calificación en la escala de la A-F

### Derechos de Estudiantes con Impedimentos:
La Universidad de Puerto Rico cumple con todas las leyes federales y estatales, y reglamentos concernientes a discriminación, incluyendo “The American Dissabilities Act” (Ley ADA) y la Ley 51 del Estado Libre Asociado de Puerto Rico. Los estudiantes que reciban servicios de rehabilitación vocacional deben comunicarse con el profesor al inicio del semestre para planificar el acomodo razonable y equipo de asistencia necesario conforme con las recomendaciones de la Oficina de Asuntos para las Personas con Impedimentos (OAPI) del Decanato de Estudiantes.


### Bibliografía:

1. A. V. Aho, J. E. Hopcroft, and J. D. Ullman, Data Structures and Algorithms (1983) Addison-Wesley (Esta es una referencia clásica)
1. T. H. Cormen, C. E. Leiserson,  R. L. Rivest, and  C. Stein, Introduction to ALGORITHMS (Second edition).(2001) The MIT Press, Mc-Graw-Hill
1. Nell Dale. C++ Plus Data Structures (Third Edition). (2003)Jones and Bartlett.
1. Behrouz Forouzan and Richard Gilberg, Computer Science: A Structured Approach Using C++ (Second edition). (2003) Brooks/Cole Publishing Company
1. Michael Main and Walter Savitch, Data Structures and other objects Using C++ (Third edition).  (2004)
1. M. Moir and N. Shavit. Concurrent Data Structures (Book Chapter). In Handbook of Data Structures and Applications, (2004) D. Metha and S.Sahni Editors, Chapman and Hall/CRC Press, Chapter 47, pages 47-1-- 47-30 (available at: http://www.cs.tau.ac.il/~shanir/concurrent-data-structures.pdf )
1. Larry R. Nyhoff, ADTs, Data Structures and Problem Solving with C++ (Second edition). (2005) Prentice Hall
1. L. Torres, Asistencia tecnológica: una posibilidad real, Isla Negra, 2002

### Referencias electrónicas:
1. Programming Abstractions (Stanford University) - http://www.stanford.edu/class/cs106b/
2. Notes for the Sedgewick's Algorithms book: http://algs4.cs.princeton.edu/home/
3. Tutorial on Classes -
  * http://www.cplusplus.com/doc/tutorial/classes/ <br>
  * http://cs.calvin.edu/books/c++/ds/2e/ <br>
  * http://sce.cl.uh.edu/murphy/DSTutorial_v1/ <br>
  * http://www.cplusplus.com/doc/language/tutorial/structures.html <br>
