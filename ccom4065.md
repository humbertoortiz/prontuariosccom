<center>
Universidad de Puerto Rico
Recinto de Río Piedras<br> Facultad de Ciencias Naturales<br>
Departamento de Ciencia de Cómputos
</center>


----

### Título: Álgebra Lineal Numérica

#### Codificación: CCOM 4065

#### Número de horas/crédito: 45 horas, 3 créditos

#### Prerequisitos y co-requisitos: CCOM 3033 y MATE 3151


----

### Descripción del curso:

En este curso se estudiarán métodos numéricos para resolver problemas de
álgebra de matrices. Se estudiarán los algoritmos y estos se
implementarán en la computadora usando una plataforma de programación de
alto nivel. Se usarán problemas de prueba para ilustrar las fortalezas y
debilidades de los métodos. Los temas que se estudiarán incluyen:
propiedades de matrices, la representación de punto flotante para
números reales, métodos para resolver sistemas lineales (directos e
iterativos), cuadrados mínimos y cálculo de valores propios de matrices.
Al final de cada tema se presentará un ejemplo de un problema de la vida
real para el cual se necesite alguno de los métodos discutidos para
resolver el problema.


### Objetivos del curso

Al finalizar el curso los estudiantes podrán:

1.  Realizar operaciones con matrices y conocer sus propiedades
    fundamentales.

2.  Representar los números reales en la computadora y calcular su
    aritmética y error de redondeo.

3.  Aplicar métodos numéricos para aproximar la solución de un sistema
    lineal de tamaño grande.

4.  Resolver problemas de cuadrados mínimos a través de los métodos
    enseñados.

5.  Calcular numéricamente los valores propios de matrices.

6.  Evaluar la contribución de los desarrollos recientes en Álgebra
    Lineal Numérica.

7.  Indicar y explicar los conceptos fundamentales del Álgebra Lineal
    Numérica.

8.  Reconocer la importancia de computar la pérdida de significación de
    punto flotante y los análisis de error al resolver sistemas
    lineales.

9.  Apreciar la importancia del Álgebra Lineal en la Ciencia de
    Cómputos.



### Bosquejo de contenido y distribución del tiempo

1. Definición y propiedades de matrices (6 horas)
<ol type="a">
<li>Definiciones y operaciones de matrices (0.75 hora).
<li>Invertibilidad de matrices (0.75 hora).
<li>Determinante de matrices (0.75 hora).
<li>Matrices partidas (0.75 hora).
<li>Independencia lineal de vectores y rango de matrices (1.5 hora).
<li>Producto interior y norma de vectores (0.75 hora).
<li>Ortogonalidad de vectores y matrices (0.75 hora).
</ol>
<br>
2. Representación en la computadora de números reales usando el punto flotante (6 horas)
<ol type="a">
<li>La representación de punto flotante (1.5 hora).
<li>Aritmética de punto flotante (1.5 hora).
<li>Pérdida de significación en la artimética de punto flotante (3 hora).
</ol>
<br>
3. Métodos directos para resolver sistemas de ecuaciones lineales. ( 12 horas)
<ol type="a">
<li>Sistemas lineales (1.5 hora).
<li>Eliminación gaussiana (1.5 horas).
<li>Variantes de eliminación gaussiana para matrices especiales (3 horas).
<li>La factorización de matrices en la forma LU (1.5 hora).
<li>Análisis de error al resolver sistemas lineales con eliminación gaussiana (1.5 horas).
<li>Norma de matrices y número condición de matrices (3 hora).  
</ol>    
<br>
4. Examen I.  (1.5 hora)  

5. Problemas de cuadrados mínimos. ( 7.5 horas )
<ol type="a">
<li>Las ecuaciones normales y sistemas de ecuaciones lineales
    rectangulares (1.5 hora).
<li> La factorización QR de una matriz (3 horas).
<li> El método de Gauss-Newton para problemas no lineales de cuadrados
    mínimos (3 hora).
</ol>
<br>
6. Métodos para calcular valores propios de matrices (6 horas)
<ol type="a">
<li>Definición y localización de valores propios (1.5 hora).
<li>Método de potencias (1.5 hora).
<li>El método de factorizaciones QR (3 horas).
</ol>
<br>
7. Examen II. ( 1.5 hora ) 

8. Métodos iterativos para resolver sistemas lineales. ( 4.5 horas)
<ol type="a">
<li>Método de Gauss-Jacobi (1.5 hora).
<li>Método de Gauss-Seidel (1.5 hora).
<li>Método de sobre relajación sucesiva (SOR) (1.5 hora).
</ol>

9. Examen Final

Total: 45 horas



### Estrategias instruccionales

El curso será dictado tipo conferencia en el salón de clase. El
contenido del curso y material de apoyo estará disponible para el
estudiante en la página web del curso. Además, se usarán actividades
interactivas virtuales, tales como foros para motivar la discusión de
los temas y tratar de aclarar dudas.

### Recursos mínimos disponibles o requeridos

Se necesita tener acceso a una computadora personal para poder acceder
los materiales del curso. Como mínimo se necesita un programa para ver
archivos en formato PDF, un navegador de la internet, un procesador de
palabras y una plataforma de programación de alto nivel, tales como
<span>matlab</span>/Octave, Python/Sage o R. En el segundo piso de la
biblioteca de Ciencias Naturales hay computadoras personales para el uso
de los estudiantes.


### Estrategias de evaluación

|  |  Porcentaje |
|---------------------------|-------|
| Pruebas cortas            | 30%   |
|   Examen I                | 20%   |
|   Examen II               |   20% |
|   Examen final            |   30% |
| Total | 100%|

Se darán pruebas cortas anunciadas los últimos 15 minutos de algunas
clases de los miércoles que se basarán en problemas que se pondrán en la
página web del curso con anterioridad.

Evaluación diferenciada a estudiantes con necesidades especiales.

### Sistema de calificación

Sistema de letras ( A, B, C, D or F ).

### Derechos de Estudiantes con Impedimentos
La Universidad de Puerto Rico cumple con todas las leyes federales y estatales, y reglamentos concernientes a discriminación, incluyendo “The American Dissabilities Act” (Ley ADA) y la Ley 51 del Estado Libre Asociado de Puerto Rico. Los estudiantes que reciban servicios de rehabilitación vocacional deben comunicarse con el profesor al inicio del semestre para planificar el acomodo razonable y equipo de asistencia necesario conforme con las recomendaciones de la Oficina de Asuntos para las Personas con Impedimentos (OAPI) del Decanato de Estudiantes.

### Bibliografía

1.  754-2008 *IEEE Standard for Floating-Point Arithmetic*, IEEE, New
    York 2008.

2.  Grégoire Allaire and Sidi Mahmoud Kaber. *Numerical Linear Algebra.
    Texts in Applied Mathematics*, Vol. 55, Springer, 2008.

3.  David Goldberg. *What Every Computer Scientist Should Know About
    Floating-Point Arithmetic*. ACM Computing Surveys 23, 5-48 1991.

4.  G. H. Golub and C. F. Van Loan. *Matrix Computations*. 3rd Ed. Johns
    Hopkins University Press, Baltimore, Md,1996.

5.  Steven J. Leon. *Linear Algebra With Applications*. Seventh Edition,
    Pearson, 2010.

6.  Michael L. Overton. *Numerical Computing with IEEE Floating Point
    Arithmetic*. SIAM Publishing, Philadelphia 2001.

7.  Y. Saad. *Iterative Methods for Sparse Linear Systems*. 2nd Ed. SIAM
    Publishing, Philadelphia, 2000.

8.  Timothy Sauer, *Numerical Analysis*, Addison Wesley, 2005.

9.  L. Torres. *Asistencia Tecnológica: derecho de todos*. Editorial
    Isla Negra, 2002.

10. Lloyd N. Trefethen and David Bau. *Numerical Linear Algebra*. SIAM
    Publishing, 1997.

11. Van Loan, C. *Introduction to Scientific Computing: A Matrix-Vector
    Approach Using MATLAB*, Prentice-Hall, New York, 1997.


### Referencias electrónicas:

1.  Robert A. Beezer. *A First Course in Linear Algebra*. Edition,
    version 3.20. Congruent Press, 2013. <http://linear.ups.edu>.

2.  M. Marcano, Introducción a MATLAB, folleto, 2007 
    <http://epsilon.uprrp.edu/mmarcano/cursos/Computational_Biology/Presentations/MatlabIntroduction.pdf>.

3.  M. Marcano, Notas de la clase, presentaciones, 2012,
    <http://epsilon.uprrp.edu/mmarcano/cursos/Numerical_Linear_Algebra/>.

4.  I. Rubio, Una introducción breve a Latex, folleto, 2011,
    <http://ccom.uprrp.edu/actividades/IntroLatex.pdf>.

5.  Gilbert Strang. 18.06 Linear Algebra, Spring 2010. (MIT
    OpenCourseWare: Massachusetts Institute of Technology),
    <http://ocw.mit.edu/courses/mathematics/18-06-linear-algebra-spring-2010>
    (Accessed 11 Feb, 2014). License: Creative Commons BY-NC-SA.
